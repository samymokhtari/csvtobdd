CREATE TABLE contacts(
    id INTEGER PRIMARY KEY,
    first_name TEXT NOT NULL
);

CREATE TABLE products(
    nom TEXT NOT NULL,
    list_price DECIMAL (10,2) NOT NULL
);