import unittest
import log
import database.bdd
db=database.bdd    

class TestCSVtoBDDFonctions(unittest.TestCase):
    def test_insertion(self):
        listeV=list()
        dico = {
            'adresse_titulaire' : '21 Ambroise Paré',
            'nom' : 'Exzeman',
            'prenom' : 'Leo', 
            'immatriculation': 'EX352LA' , 
            'date_immatriculation': '07/09/1995', 
            'vin' : '08ABCDEFG123456789', 
            'marque': 'Renault Clio 2', 
            'denomination_commerciale':'RC2', 
            'couleur': 'Noir comme le desespoir', 
            'carrosserie': 'Carbone',
            'categorie': 'Poids lourd', 
            'cylindre': 2, 
            'energie': 2 , 
            'places': 8, 
            'poids': 1, 
            'puissance': 45,
            'type': '.Exzema',
            'variante': '92-12185849',
            'version': '123456789'
        }

        listeV.append(dico)

        connect=db.creationBDD('tests/test.sqlite3')
        cursor=db.connexionBDD(connect)
        db.creationTable(connect,cursor)
        db.majdonnees(connect,cursor,listeV,0)
        cursor.execute("""SELECT count(*) from SIV where immatriculation='EX352LA'""")
        rows=cursor.fetchone()
        connect.commit()
        self.assertEqual(rows[0],1)

    def test_modification(self):
        listeV=list()
        dico = {
            'adresse_titulaire' : '21 Ambroise Paré',
            'nom' : 'Exzeman',
            'prenom' : 'Leo', 
            'immatriculation': 'EX352LA' , 
            'date_immatriculation': '07/09/1995', 
            'vin' : '08ABCDEFG123456789', 
            'marque': 'Peugeot 206 RC', 
            'denomination_commerciale':'RC2', 
            'couleur': 'Noir comme le desespoir', 
            'carrosserie': 'Carbone',
            'categorie': 'Poids lourd', 
            'cylindre': 2, 
            'energie': 2 , 
            'places': 8, 
            'poids': 1, 
            'puissance': 45,
            'type': '.Exzema',
            'variante': '92-12185849',
            'version': '123456789'
        }
        listeV.append(dico)
        connect=db.creationBDD('tests/test.sqlite3')
        cursor=db.connexionBDD(connect)
        db.creationTable(connect,cursor)
        db.majdonnees(connect,cursor,listeV,0)
        cursor.execute("""SELECT count(*) from SIV where immatriculation='EX352LA'""")
        rows=cursor.fetchone()
        connect.commit()
        self.assertEqual(rows[0],1)
    
    def test_generationLog(self):
        log.ecritureLogs('tests/test.log','info','test OK')
        with open('tests/test.log') as fic:
            line = fic.readline()
        self.assertEqual(line.find('test OK')+1,33)
