import logging

def ecritureLogs(file,typemsg,message):
    logging.basicConfig(filename=file, level=logging.DEBUG,format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filemode='w')
    if(typemsg=='info'):
        logging.info(message)
    elif(typemsg=='error'):
        logging.error(message)
    elif(typemsg=='critical'):
        logging.critical(message)
    elif(typemsg=='warning'):
        logging.warning(message)
    elif(typemsg=='debug'):
        logging.debug(message)
    