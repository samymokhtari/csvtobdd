import stockage
import csv
import os
import log

def LectureFichier(listeVehicule,path):
    log.ecritureLogs('logs.log','debug',"Tentative d'acces au fichier %s..."%(path))
    if os.path.exists(path) and os.path.isfile(path):
        with open(path, newline='') as csvfile:
            read = csv.reader(csvfile, delimiter=';')
            for row in read:
                stockage.StockageDonnee(listeVehicule,row)
            log.ecritureLogs('logs.log','info',"Lecture du fichier %s OK."%(path))
    else:
        log.ecritureLogs('logs.log','error',"Erreur de lecture du fichier %s"%(path))