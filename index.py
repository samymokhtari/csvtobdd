import sys
import sqlite3
import csv
import lecture
import database.bdd
import log

def main():    
    # DEBUT DU PROGRAMME
    log.ecritureLogs('logs.log','info','DEBUT SCRIPT ***CSVtoBDD*** ...')

    # INITIALISATION DES DONNÉES
    listeVehicule=[]
    db=database.bdd
    path="database/automobile.sqlite3"
    index=modificationCompteur=insertionCompteur=0

    #LECTURE ET STOCKAGE DU CSV
    lecture.LectureFichier(listeVehicule,'data.csv')

    #CRÉATION DE LA BDD ET DE LA TABLE
    connect=db.creationBDD(path)
    cursor=db.connexionBDD(connect)
    db.creationTable(connect, cursor)

    #MODIFICATION ET INSERTION DE DONNEES DANS LA BDD
    for index in range(0,len(listeVehicule)):
            if(db.majdonnees(connect, cursor, listeVehicule,index)==1):
                insertionCompteur=insertionCompteur+1
            else:
                modificationCompteur=modificationCompteur+1

    #ECRITURE DES LOGS
    log.ecritureLogs('logs.log','debug',"%d insertion(s) effectue(s)."%insertionCompteur)
    log.ecritureLogs('logs.log','debug',"%d modification(s) effectue(s)."%modificationCompteur)

    #DECONNEXION DE LA BDD
    db.deconnexionBDD(connect)

    #FIN PROGRAMME
    log.ecritureLogs('logs.log','info','FIN SCRIPT.')


if __name__ == "__main__":
    # execute only if run as a script
    main()
