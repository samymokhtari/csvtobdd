import log
import sys

#Fonction initialisation de la structure de données (Liste de dictionnaire)
def StockageDonnee(listeVehicule, ligne):
    try:
        dico = {
            'adresse_titulaire' : ligne[0],
            'nom' : ligne[1],
            'prenom' : ligne[2], 
            'immatriculation': ligne[3] , 
            'date_immatriculation': ligne[4], 
            'vin' : ligne[5], 
            'marque': ligne[6], 
            'denomination_commerciale':ligne[7], 
            'couleur': ligne[8], 
            'carrosserie': ligne[9],
            'categorie': ligne[10], 
            'cylindre': ligne[11], 
            'energie': ligne[12] , 
            'places': ligne[13], 
            'poids': ligne[14], 
            'puissance': ligne[15],
            'type': ligne[16],
            'variante': ligne[17],
            'version': ligne[18]
        }
        listeVehicule.append(dico)
    except Error as e:
        print(e)
        log.ecritureLogs('logs.log','error','Echec du stockage des donnees.')
