import sqlite3 
import log
import sys
from sqlite3 import Error

def creationBDD(path):
    try:
        connection = sqlite3.connect(path)
    except Error as e:
        print(e)
        log.ecritureLogs('logs.log','critical','Echec de creation de la base de donnees.')
        sys.exit(1)
    
    log.ecritureLogs('logs.log','info','Acces a la base de donnees OK.')
    return connection

def connexionBDD(connection):
    try:
        cursor = connection.cursor()
    except Error as e:
        print(e)
        log.ecritureLogs('logs.log','critical','Echec de connexion de la base de donnees.')
        sys.exit(2)

    log.ecritureLogs('logs.log','info','Connexion a la base de donnees OK.')
    return cursor

def deconnexionBDD(connection):
    connection.close()
    log.ecritureLogs('logs.log','info','Deconnexion de la base de donnees OK.')

def creationTable(connection, cursor):
    try:
        log.ecritureLogs('logs.log','debug','Tentative de creation de la table SIV...')
        cursor.execute("""CREATE TABLE IF NOT EXISTS SIV(
            adresse_titulaire text NOT NULL,
            nom text, 
            prenom text NOT NULL,
            immatriculation text PRIMARY KEY,
            date_immatriculation text NOT NULL,
            vin integer NOT NULL,
            denomination_commerciale text NOT NULL,
            couleur text NOT NULL,
            categorie text NOT NULL,
            cylindre integer NOT NULL,
            energie integer NOT NULL,
            poids integer NOT NULL,
            puissance integer NOT NULL,
            type text NOT NULL,
            variante text NOT NULL,
            version integer NOT NULL)
        """)
        connection.commit()    
    except Error as e:
        print(e)
        log.ecritureLogs('logs.log','critical','Echec de creation de la table SIV.')
        sys.exit(3)
    log.ecritureLogs('logs.log','info','Acces a la table SIV OK.')


def insertion(connection, cursor, listeVehicule,index):
    cursor.execute("""INSERT INTO SIV (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, denomination_commerciale, couleur, categorie, cylindre, energie, poids, puissance, type, variante, version) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", (listeVehicule[index]['adresse_titulaire'], listeVehicule[index]['nom'], listeVehicule[index]['prenom'], listeVehicule[index]['immatriculation'], listeVehicule[index]['date_immatriculation'],listeVehicule[index]['vin'],listeVehicule[index]['denomination_commerciale'], listeVehicule[index]['couleur'], listeVehicule[index]['categorie'], listeVehicule[index]['cylindre'],listeVehicule[index]['energie'],listeVehicule[index]['poids'],listeVehicule[index]['puissance'],listeVehicule[index]['type'],listeVehicule[index]['variante'],listeVehicule[index]['version'])) 
    connection.commit()

def majdonnees(connection, cursor, listeVehicule,index):
    cursor.execute("""SELECT * FROM SIV WHERE(immatriculation = ?)""",(listeVehicule[index]['immatriculation'],))
    if (cursor.fetchone() == None):
        insertion(connection, cursor, listeVehicule,index)
        return 1 # Retourne 1 si l'opération de MAJ à été une insertion
    else:
        modification(connection, cursor, listeVehicule, index)
        return 2 # Retourne 2 si l'opération de MAJ à été une modification

def modification(connection, cursor, listeVehicule,index):
    cursor.execute("""UPDATE SIV SET adresse_titulaire=?, nom=?, prenom=?, immatriculation=?, date_immatriculation=?, vin=?, denomination_commerciale=?, couleur=?, categorie=?, cylindre=?, energie=?, poids=?, puissance=?, type=?, variante=?, version=?
    WHERE immatriculation = ? """,(listeVehicule[index]['adresse_titulaire'], listeVehicule[index]['nom'], listeVehicule[index]['prenom'], listeVehicule[index]['immatriculation'], listeVehicule[index]['date_immatriculation'],listeVehicule[index]['vin'],listeVehicule[index]['denomination_commerciale'], listeVehicule[index]['couleur'], listeVehicule[index]['categorie'], listeVehicule[index]['cylindre'],listeVehicule[index]['energie'],listeVehicule[index]['poids'],listeVehicule[index]['puissance'],listeVehicule[index]['type'],listeVehicule[index]['variante'],listeVehicule[index]['version'],listeVehicule[index]['immatriculation'])) 
    connection.commit()