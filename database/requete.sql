CREATE TABLE IF NOT EXISTS SIV(
    adresse_titulaire text NOT NULL,
    nom text, 
    prenom text NOT NULL,
    immatriculation text NOT NULL,
    date_immatriculation text NOT NULL,
    vin integer NOT NULL,
    denomination_commerciale text NOT NULL,
    couleur text NOT NULL,
    categorie text NOT NULL,
    cylindre integer NOT NULL,
    energie integer NOT NULL,
    poids integer NOT NULL,
    puissance integer NOT NULL,
    type text NOT NULL,
    variante text NOT NULL,
    version integer NOT NULL
);